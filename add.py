from pydantic import BaseModel
from uuid import uuid4

from token_gen import BaseTokenModel


class AddIn(BaseTokenModel):
    name: str
    description: str = None
    time_created: str
    completed: bool = False


class AddOut(BaseModel):
    uid: str


async def handleAdd(add_in, db):
    """
    Need to handle the incoming requests to add a todo task.

    We need the string and time_created and we will generate an
    Unique ID for the task and return that. Other methods will
    depend on this ID in order to operate on this task.
    """
    id = uuid4()
    add_in = add_in.dict()
    add_in["uid"] = str(id)

    db.tasks.insert_one(add_in)

    return AddOut(uid=str(id))
