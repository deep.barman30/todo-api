"""
Handle the token related stuff of the API.
"""

from pydantic import BaseModel, validator, ValidationError
from secrets import compare_digest, token_urlsafe


token_db = []


def is_token_valid(token_passed):
    """
    Check if the passed token is valid or not.
    """
    # Check if the token is presnt in the token
    # DB
    return token_passed in token_db


def add_token(token_passed):
    """
    Add the passed token to the token DB
    """
    # Put a check to see if the token is already present
    # This should hardly be an issue since the token will
    # be generated after verification that a new token
    # doesn't exist.
    if token_passed in token_db:
        return False
    token_db.append(token_passed)
    return True


# Define a base class that will handle token stuff
class BaseTokenModel(BaseModel):
    token: str

    @validator("token")
    def verify_token(cls, value):
        if not is_token_valid(value):
            raise ValidationError("Token not Valid")
        return value


class TokenIn(BaseModel):
    username: str
    password: str


class TokenOut(BaseModel):
    status: str
    token: str = None


def handleToken(token_in):
    token_in = token_in.dict()
    correct_username = compare_digest(token_in["username"], "deepjyoti30")
    correct_password = compare_digest(token_in["password"], "justapw123")

    if not (correct_username and correct_password):
        return TokenOut(status="Incorrect username / password")

    created_token = token_urlsafe()

    add_token(created_token)
    return TokenOut(status="OK", token=created_token)
