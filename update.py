from pydantic import BaseModel

from token_gen import BaseTokenModel


class UpdateIn(BaseTokenModel):
    uid: str
    completed: bool


class UpdateOut(BaseModel):
    matched: int
    modified: int


async def handleUpdate(update_in, db):
    """
    Handle the update request. We need to update the status
    of the task.
    """
    update_in = update_in.dict()

    status = await db.tasks.update_one(
                        {'uid': update_in["uid"]},
                        {"$set": {'completed': update_in["completed"]}}
                    )

    return UpdateOut(
                matched=int(status.matched_count),
                modified=int(status.modified_count)
        )
