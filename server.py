from fastapi import FastAPI
from motor import motor_tornado
from os import environ

from add import AddIn, AddOut, handleAdd
from delete import DeleteIn, DeleteOut, handleDelete
from update import UpdateIn, UpdateOut, handleUpdate
from modify import ModifyIn, ModifyOut, handleModify
from token_gen import TokenIn, TokenOut, handleToken

DB_URL = environ.get("mongo_todoURL")

app = FastAPI()

client = motor_tornado.MotorClient(DB_URL)
db = client.tododb


@app.post("/generate_token/", response_model=TokenOut)
async def token_handler(token_in: TokenIn):
    response = handleToken(token_in)
    return response


@app.post("/add/", response_model=AddOut)
async def add_handler(add_in: AddIn):
    response = await handleAdd(add_in, db)
    return response


@app.post("/delete/", response_model=DeleteOut)
async def delete_handler(delete_in: DeleteIn):
    response = await handleDelete(delete_in, db)
    return response


@app.post("/update/", response_model=UpdateOut)
async def update_handler(update_in: UpdateIn):
    response = await handleUpdate(update_in, db)
    return response


@app.post("/modify/", response_model=ModifyOut)
async def modify_handler(modify_in: ModifyIn):
    response = await handleModify(modify_in, db)
    return response
