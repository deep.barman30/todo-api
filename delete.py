from pydantic import BaseModel

from token_gen import BaseTokenModel


class DeleteIn(BaseTokenModel):
    uid: str


class DeleteOut(BaseModel):
    delete_count: str


async def handleDelete(delete_in, db):
    """
    The task will be deleted only when the ID matches.

    Once the task is deleted, the status is returned.
    """
    status = await db.tasks.delete_one(delete_in.dict())

    return DeleteOut(
                delete_count=str(status.deleted_count),
            )
