from pydantic import BaseModel

from token_gen import BaseTokenModel


class ModifyIn(BaseTokenModel):
    uid: str
    name: str = None
    description: str = None


class ModifyOut(BaseModel):
    matched: int
    modified: int


async def handleModify(modify_in, db):
    """
    We need to modify the passed data.

    Both name and description are optional.
    We won't be handling completion requests through
    this endpoint.
    """
    modify_in = modify_in.dict()
    uid = modify_in["uid"]

    # Remove the uid from the updated data dict
    modify_in.pop("uid")

    # We also need to make sure the None objects are not
    # passed.
    modify_in = {key: val for key, val in modify_in.items() if val is not None}

    status = await db.tasks.update_one(
                            {"uid": uid},
                            {"$set": modify_in}
                    )

    return ModifyOut(
                matched=int(status.matched_count),
                modified=int(status.modified_count)
            )
