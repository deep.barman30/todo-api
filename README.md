# Basic ToDo Api using MongoDB and Python

## Requirements

- FastApi
- motor

## Prerequisites

The app expects an environment variable of the name ```mongo_todoURL``` which would be a valid mongoDB connection URI. It can be one of the following

- A localhost URL ```mongodb://localhost:27017```

- A replica set ```mongodb://host1,host2/?replicaSet=my-replicaset-name```

- A mlab URL ```mongodb+srv://<username>:<password>@cluster0-m5vl9.mongodb.net/<dbname>?retryWrites=true&w=majority```

The default username and password to generate a token is ```deepjyoti30``` and ```justapw123```.
Since it's just a demo API, it's hardcoded right now.

## Installation

Install the requirements by using the requirements file.

```pip install -r requirements.txt```

Run the server using the following command

```uvicorn server:app --reload```

Once that's done goto [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs) in order to try out the API.
